<?php

namespace Easytek\EcmsBundle\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

class EasytekEcmsTwigExtension extends \Twig_Extension
{
	private $container;
	
	/**
	 * @param ContainerInterface $container A container.
	 */
	public function __construct($container)
	{
		$this->container = $container;
	}
	
    public function getFunctions()
    {
		// TODO : Trouver comment intégrer le "raw"
        return array(
        	'web_configuration' => new \Twig_Function_Method($this, 'webConfiguration'),
            'csrf_post' => new \Twig_Function_Method($this, 'csrf_post'),
//         	'content_slot' => new \Twig_Function_Method($this, 'content_slot'),
        	'router_params' => new \Twig_Function_Method($this, 'routerParams'),
        );
    }
    
    public function webConfiguration($bundleName, $key)
    {
    	return $this->container->get('ecms.configuration')->get($bundleName, $key);
    }
	
	public function csrf_post(\Symfony\Component\Form\FormView $form, $confirmMessage = 'Êtes-vous sûr ?')
	{
		$csrfToken = $form->children['_token']->vars['value'];
		$id = $form->children['id']->vars['value'];
		
		// TODO : Déplacer le JS dans un template Twig
		$js = "javascript:
		".($confirmMessage !== false ? "if (confirm('".$confirmMessage."')) {":"")."
			var f = document.createElement('form');
			f.style.display = 'none';
			this.parentNode.appendChild(f);
			f.method = 'post';
			f.action = this.href;
			var m = document.createElement('input');
			m.setAttribute('type', 'hidden');
			m.setAttribute('name', 'form[id]');
			m.setAttribute('value', '".$id."');
			f.appendChild(m);var m = document.createElement('input');
			m.setAttribute('type', 'hidden');
			m.setAttribute('name', 'form[_token]');
			m.setAttribute('value', '".$csrfToken."');
			f.appendChild(m);f.submit();
		".($confirmMessage !== false ? '};':'')."
		return false;
		";
		
		return 'onclick="'.$js.'"';
	}

// 	public function content_slot($content)
// 	{
// 		// TODO : Déplacer le HTML dans un template Twig
// 		return "<div class=\"page{% if is_granted('ROLE_ADMIN') %} editable{% endif %}\"{% if is_granted('ROLE_ADMIN') %} data-id=\"{{ entity.id }}\"{% endif %}>".$content."</div>";
// 	}

	/**
	 * Emulating the symfony 2.1.x $request->attributes->get('_route_params') feature.
	 * Code based on PagerfantaBundle's twig extension.
	 */
	// commenté vu qu'on est en 2.1 maintenant
// 	public function routerParams()
// 	{
// 		$router = $this->container->get('router');
// 		$request = $this->container->get('request');
	
// 		$routeName = $request->attributes->get('_route');
// 		$routeParams = $request->query->all();
// 		foreach ($router->getRouteCollection()->get($routeName)->compile()->getVariables() as $variable) {
// 			$routeParams[$variable] = $request->attributes->get($variable);
// 		}
	
// 		return $routeParams;
// 	}
	
	
	public function getName()
	{
		return 'my_twig_extension';
	}
}