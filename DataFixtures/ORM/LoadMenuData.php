<?php

namespace Easytek\EcmsBundle\DataFixtures\ORM;

use Easytek\EcmsBundle\Entity\MenuItem;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easytek\EcmsBundle\Entity\Page;

class LoadMenuData extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager)
    {
    	$rootMenuItem = new MenuItem();
    	$rootMenuItem->setName('menu1');
    	
    	$this->addReference('rootMenuItem', $rootMenuItem);
    	
    	$menuItems = array();
    	
    	$menuItem = new MenuItem();
    	$menuItem
    		->setName('Accueil')
    		->setUri('/page/1-accueil')
    		->setParent($rootMenuItem)
		;

        $manager->persist($rootMenuItem);
        $manager->persist($menuItem);
        $manager->flush();
    }
    
    public function getOrder()
    {
    	return 1;
    }
}