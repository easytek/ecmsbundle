<?php

namespace Easytek\EcmsBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easytek\EcmsBundle\Entity\Page;

class LoadPageData extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager)
    {
    	$pages = array();
    	
        $page = new Page();
        $page
        	->setInternalTitle('Accueil')
        	->setHtml("<h2>".$page->getInternalTitle()."</h2>
        				<p>Ibi victu recreati et quiete, postquam abierat timor, vicos opulentos adorti equestrium adventu cohortium, quae casu <strong>propinquabant</strong>, nec resistere planitie porrecta conati digressi sunt retroque concedentes omne iuventutis robur <strong>relictum </strong>in sedibus acciverunt.Quae dum ita struuntur, indicatum est apud Tyrum indumentum regale textum occulte, incertum quo locante vel cuius usibus <strong>apparatum</strong>. ideoque rector provinciae tunc pater</p>
        	")
        ;
        
        $pages[] = $page;
        
        $page = new Page();
        $page
        	->setInternalTitle('Mentions légales')
        	->setHtml("<h2>".$page->getInternalTitle()."</h2>
	        			<p>L'éditeur du site {{ site }} est {{ société }} au capital de {{ capital }} euros, son siège social est situé au {{ siege social }}, et est immatriculée au Registre du Commerce et des Sociétés de {{ ville rcs }} sous le numéro {{ rcs }}.
						<p>Directeur de la publication : {{ directeur publication }}</p>
						<p>Le site web est hébergé par l'éditeur.</p>
						<p>Ce site respecte le droit d'auteur. Tous les droits des auteurs des Oeuvres protégées reproduites et communiquées sur ce site, sont réservés. Sauf autorisation, toute utilisation des Oeuvres autres que la reproduction et la consultation individuelles et privées sont interdites.</p>
						<p>Afin d'optimiser l'usage du site web, des cookies peuvent être placé sur les ordinateurs des utilisateurs du site web. Ces cookies sont notamment utilisés pour faciliter l'accès des utilisateurs à leurs comptes ainsi que pour afficher des bandeaux publicitaires adaptés à leur manière de naviguer sur internet. Pour savoir comment refuser, supprimer ces \"Cookies\" ou être prévenu de leur réception par un message, veuillez consulter la rubrique d'aide de votre navigateur internet.</p>
        	");
        ;
        
        $pages[] = $page;

        foreach($pages as $page)
	        $manager->persist($page);
        
        $manager->flush();
    }
    
    public function getOrder()
    {
    	return 1;
    }
}
