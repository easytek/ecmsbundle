<?php

namespace Easytek\EcmsBundle\DataFixtures\ORM;

use Easytek\EcmsBundle\Entity\User;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easytek\EcmsBundle\Entity\Page;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager)
    {
        $user = new User();
        $user
        	->setUsername('admin')
	        ->setPlainPassword('admin')
    		->setEmail('contact@easytek.fr')
        	->setRoles(array('ROLE_SUPER_ADMIN'))
        	->setEnabled(true)
		;
        
        $manager->persist($user);
        $manager->flush();
    }
    
    public function getOrder()
    {
    	return 1;
    }
}