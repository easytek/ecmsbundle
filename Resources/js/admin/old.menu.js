$(function(){
	
	//####{ Models & Collection }####
	
	MenuItems = Backbone.Collection.extend({});
	
	MenuItem = Backbone.Model.extend({
		id: null,
		name: null,
		uri: null,
		
		initialize: function(){
			this.url = globals.frontController+'/menu/'+globals.csrfTokens.menu+'/'+this.id;
			
            this.bind("error", function(model, error){
                console.log(error);
            });
		},
	
		move: function(method, destinationEntityId){
			var obj = this;
			
			jqXHR = $.getJSON(
				globals.frontController+'/menu/'+globals.csrfTokens.menuSortable+'/'+this.id+'/'+method+'/'+destinationEntityId,
				function(data){
					if(data.result == 1){
						console.log('Le MenuItem #'+obj.id+' a bien été déplacée en '+method+' #'+destinationEntityId+'.');
						obj.trigger('move');
					} else {
						console.log('Une erreur est survenue lors du déplacement du MenuItem #'+obj.id+' en '+method+' #'+destinationEntityId+'.');
					}
				}
			);
		}
	});
	
	//####{ Views }####
	
	//--- Liste publique représentant le menu sur le site.
	MenuView = Backbone.View.extend({
		el: null,
		collection: null,
		
		initialize: function(){
			this.collection.bind('change', this.update, this);
			this.collection.bind('destroy', this.remove, this);
			this.collection.bind('add', this.refresh, this);
			this.collection.bind('move', this.refresh, this);
		},
		
		update: function(menuItem){
			var item = this.findElement(menuItem);
			
			item.attr('href', menuItem.get('uri'));
			item.text(menuItem.get('name'));
		},
		
		remove: function(menuItem){
			var target = this.findElement(menuItem).closest('li');
			target.fadeOut(function(){ target.remove(); });
		},
		
		refresh: function(callback){
			var obj = this;
			
			$.get(globals.frontController + '/menu/show', function(data){
				obj.el.html($(data));
			});
		},
		
		findElement: function(menuItem){
			return this.el.find('li a[data-id="'+menuItem.get('id')+'"]');
		}
	});
	
	//--- Dialogs d'admin du menu
	MenuDialogsView = Backbone.View.extend({
		el: null,
		collection: null,
		
		initialize: function(){
			var obj = this;
			
			this.el.find('a[data-confirm]').each(function(){
				$(this).click(function(){
					return confirm($(this).attr('data-confirm'));
				});
			});
			
			// Liens internes
			this.el.find('a[rel="internal"]').each(function(){
				obj.initLink($(this));
			});
			
			// Formulaires
			this.el.find('form').each(function(){
				obj.initForm($(this));
			});
			
			this.initList();
		},
		
		show: function(){
			this.el.toggle();
//			this.el.slideToggle(); // fait bugger jquery quand on a replaceWith entre temps
		},
		
		open: function(uri){
			var target = this.el.children('li[data-internal-uri="'+uri+'"]');
//			menuAdminDialog.find('li.dialog:visible').fadeOut(100, function(){ target.fadeIn(100); });
//			menuAdminDialog.find('li.dialog:visible').slideUp(100, function(){ target.slideDown(100); });
			this.el.children('li:visible').hide();
			target.show();
		},
		
		refresh: function(callback){
			var obj = this;
			
			$.get(globals.frontController + '/menu/getAdminDialogs', function(data){
				var newEl = $(data).show();
				obj.el.replaceWith(newEl);
				obj.el = newEl;
				
				obj.initialize();
			});
		},
		
		refreshDialog: function(uri, html){
			var obj = this;
			
			var dialog = this.getDialog(uri);
			
			dialog.html(html);

			dialog.find('a[rel="internal"]').each(function(){
				obj.initLink($(this));
			});
			
			dialog.find('form').each(function(){
				obj.initForm($(this));
			});
		},
		
		getDialog: function(uri){
			return this.el.children('li[data-internal-uri="'+uri+'"]');
		},
		
		initLink: function(link){
			var obj = this;
			
			link.click(function(event){
				event.preventDefault();
				var uri = $(this).attr('href');
				obj.open(uri);
			});
		},
		
		initForm: function(form){
			var crudAction = form.closest('li').attr('data-crud');
			var dialog = form.closest('li[data-internal-uri]').attr('data-internal-uri');
			
			if(crudAction == 'create')
				this.initNewForm(form, dialog);
			else if(crudAction == 'update')
				this.initEditForm(form, dialog);
		},
		
		initNewForm: function(form, dialog){
			var obj = this;
			
			var button = form.find('button');
			button.attr("disabled", false);
			
			form.submit(function(event){
				event.preventDefault();
				
				button.attr("disabled", true);
				
				var dialogURI = $(this).closest('li').attr('data-internal-uri');
	
				$.post(
					form.attr('action'),
					form.serialize(),
					function(data){
						if(data.result == 1){
							// refresh du dialog "index" (la liste)
							obj.collection.add(new MenuItem(data.entity));
							obj.refresh();
						} else {
							// refresh du formulaire en cas d'erreurs
							obj.refreshDialog(dialogURI, data.html);
						}
					}
				, 'json');		
			});
		},
		
		initEditForm: function(form){
			var obj = this;
			
			var button = form.find('button');
			button.attr("disabled", false); // pour éviter le bug du cache navigateur
			
			form.submit(function(event){
				event.preventDefault();
				
				button.attr("disabled", true);
				
				var dialogURI = $(this).closest('li').attr('data-internal-uri');
	
				$.post(
					form.attr('action'),
					form.serialize(),
					function(data){
						if(data.result == 1){
							obj.collection.get(data.entity.id).set(data.entity);
							obj.open(data.redirect);
						} else {
							obj.refreshDialog(dialogURI, data.html);
						}
						
						button.attr("disabled", false);
					}
				, 'json');		
			});
		},
		
		initList: function(){
			var obj = this;

			// Création des vues liées à chaque élément de la liste administrable
			$('#menu-editable li').each(function(){
				var id = $(this).children('div').attr('data-id');
				
				new MenuItemListAdminView({
					model: obj.collection.get(id),
					el: $(this)
				});
			});
			
			this.nestedSet();
		},
		
		nestedSet: function(){
			var obj = this;
			
			$('#menu-editable ol').nestedSortable({
				disableNesting: 'no-nest',
				forcePlaceholderSize: true,
				handle: '.deplacer',
				helper:	'clone',
				items: 'li',
				opacity: .6,
				placeholder: 'placeholder',
				revert: 250,
				tabSize: 25,
				maxLevels: 2,
				tolerance: 'pointer',
				toleranceElement: '> div',
				stop: function(event, ui){
					var li = ui.item;
					var entityId = li.children('div').attr('data-id');
					var ol = li.parent('ol');
					
		 			if(li.prev().size() > 0){
						var method = 'moveAsNextSiblingOf';
						var destinationEntityId = li.prev().children('div').attr('data-id'); // précédent
					} else {
						var method = 'moveAsFirstChildOf';
						var destinationEntityId = ol.closest('li').children('div').attr('data-id'); // parent
						if(typeof(destinationEntityId) == 'undefined')
							var destinationEntityId = ol.attr('data-root-id');
					}
		 			
		 			obj.collection.get(entityId).move(method, destinationEntityId);
				}
			});
		},
	});
	
	// Element de la liste administrable du menu
	MenuItemListAdminView = Backbone.View.extend({
		model: null,
		el: null,
		
		initialize: function(){
			this.model.bind('destroy', this.clear, this);
			this.model.bind('change', this.update, this);
		},
	
		events: {
			'click .delete': 'remove'
		},
		
		remove: function(){
			this.model.destroy();
			return false;
		},
		
		clear: function(){
			var obj = this;
			this.el.fadeOut(function(){ obj.el.remove(); });
		},
		
		update: function(){
			this.el.find('div[data-id="'+this.model.get('id')+'"] > span.name').text(this.model.get('name'));
			return false;
		},
	});
	
	//####{ Instanciations }####
	
	appView = new AppView();
	
	var menuItems = new MenuItems(); // nouvelle collection vide
	
	// Instanciations des models à partir du DOM
	$('#menu-editable li').each(function(){
		var id = $(this).children('div').attr('data-id');
		var name = $(this).children('div').children('span').text();
		var uri = "CAN'T FIND URI YET";
		
		// model
		var menuItem = new MenuItem({
			id: id,
			name: name,
			uri: uri
		});
		
		// remplissage de la collection
		menuItems.add(menuItem); 
	});
	
	if($('#menu').length > 0)
		var menuView = new MenuView({
			el: $('#menu'),
			collection: menuItems,
		});
	
	var menuDialogsView = new MenuDialogsView({
		el: $('#menu-dialogs'),
		collection: menuItems,
	});
	
	$("a[rel='menu-dialogs']").click(function(){
		menuDialogsView.show();
		$(this).toggleClass('selected');
		return false;
	});
});