$(function(){
		AppView = Backbone.View.extend({
		el: $('body'),
		loader: $('#admin-toolbar .loader'),
		
		initialize: function(){
			this.loader.ajaxStart(function(){
				$(this).fadeIn();
			});
			
			this.loader.ajaxStop(function(){
				$(this).fadeOut();
			});
			
			$('#admin-toolbar .ajax-error').ajaxError(function(){
				$(this).fadeIn();
			});
			
			$('#admin-toolbar').dropdown();
		},
	});
});