$(function(){
	$('#menu-editable ol').nestedSortable({
		disableNesting: 'no-nest',
		forcePlaceholderSize: true,
		handle: '.deplacer',
		helper:	'clone',
		items: 'li',
		opacity: .6,
		placeholder: 'placeholder',
		revert: 250,
		tabSize: 25,
		maxLevels: 2,
		tolerance: 'pointer',
		toleranceElement: '> div',
		stop: function(event, ui){
			var li = ui.item;
			var entityId = li.children('div').attr('data-id');
			var ol = li.parent('ol');
			var currentId = li.children('.item').data('id');
			
 			if (li.prev().size() > 0) {
				var method = 'moveAsNextSiblingOf';
				var destinationEntityId = li.prev().children('div').attr('data-id'); // précédent
			} else {
				var method = 'moveAsFirstChildOf';
				var destinationEntityId = ol.closest('li').children('div').attr('data-id'); // parent
				
				if (typeof(destinationEntityId) == 'undefined') {
					var destinationEntityId = ol.attr('data-root-id');
				}
			}
 			
 			var apiUrl = globals.frontController + '/menu/item/' + currentId + '/' + method + '/' + destinationEntityId;
 			
 			$.ajax({
 				  type: "POST",
 				  url: apiUrl,
// 				  data: data,
// 				  success: success,
// 				  dataType: 'JSON'
 			})
 				.success(function(){
					console.log('Le MenuItem #' + currentId + ' a bien été déplacée en ' + method + ' #' + destinationEntityId + '.');
				})
 				.fail(function(data){
 					console.log('Une erreur est survenue lors du déplacement du MenuItem #' + currentId + ' en ' + method + ' #' + destinationEntityId + '.');
// 					console.log(data);
 				})
 			;
		}
	});
});