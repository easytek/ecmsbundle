$(function(){
	
	// Permet de choisir le mod d'édition d'une page
	
	$('#switch-html-editor').click(function(){
		$($(this).attr('href')).show();
		$($('#switch-wysiwyg-editor').attr('href')).hide();
		return false;
	});
	
	$('#switch-wysiwyg-editor').click(function(){
		$($(this).attr('href')).show();
		$($('#switch-html-editor').attr('href')).hide();
		return false;
	});
	
	globals.ckeditor = {
		contentsCss: new Array()
	};
	
	$('head').find('link[rel="stylesheet"]').each(function(){
		globals.ckeditor.contentsCss.push($(this).attr('href'));
	});
	
	var ecmsbundlePath = globals.frontController + 'bundles/easytekecms';
	
	globals.ckeditor.contentsCss.push(ecmsbundlePath+'/ckeditor/ckeditor.css');
	
	globals.ckeditor.config = {
		toolbar: 'Page',
		sharedSpaces: { top : 'wysiwyg-shareplace' },
		customConfig: ecmsbundlePath+'/ckeditor/config.js',
		skin: 'ecms,'+ ecmsbundlePath+'/ckeditor/skins/ecms/',
		contentsCss: globals.ckeditor.contentsCss,
		startupFocus: true,
	};
	
	globals.ckeditor.callback = function(){ $('#wysiwyg-toolbar').trigger('open'); };
	
	
	// TODO : Faire plus proprement la gestion du wysiwyg entre textarea et bloc de contenu éditable
	// Conversion classic de textarea
	$('.wysiwyg').each(function(){
		var ckeditorConfig = globals.ckeditor.config;
		var toolbar = $(this).attr('data-wysiwyg-toolbar');

		if(toolbar != undefined)
			ckeditorConfig.toolbar = toolbar;
			
		$(this).ckeditor(globals.ckeditor.callback, ckeditorConfig);
	});
	
	// Bloc de contenu éditable au clic
	$('.editable').each(function(){
		//--- Mise en évidence visuel d'une zone éditable
		var highlight = $('<div />')
			.css('width', $(this).width())
			.css('height', $(this).height()+1)
			.addClass('editable-highlight')
			.insertBefore($(this))
		;
		
		$(this).hover(
			function(){
				highlight
					.addClass('hover')
					.css('width', $(this).width())
					.css('height', $(this).height()+1)
				;
			},
			function(){ highlight.removeClass('hover'); }
		);
		//---
		
		$(this).click(function(){
			currentEditor = $('.editable.focus');
			highlight.hide();
			
			// Si une instance de l'editeur est déjà actif on la détruit
			if(currentEditor.length > 0) {
				currentEditor.removeClass('focus');
				currentEditor.ckeditorGet().destroy();
			}

			$(this).addClass('focus'); // Pour identifier facilement par la zone éditable en cours d'édition
			$(this).ckeditor(globals.ckeditor.callback, globals.ckeditor.config);
			$(this).focus();
			
			// Activation des boutons d'actions spécifique aux bloc de contenu editable
			if($(this).attr('data-id') == undefined)
				$('.create').show();
			else
				$('.update').show();
			
			$('.close').show();
		});
	});
	
	$('#wysiwyg-toolbar').bind('close', function(){
		$(this).hide();
		var element = $('.editable.focus');
		element.ckeditorGet().destroy();
		element.removeClass('focus');
		element.prev('.editable-highlight').show();
		
		$('body').css(
			'padding-top',
			parseInt($('body').css('padding-top'))-25+'px'
		);
	});
	
	$('#wysiwyg-toolbar').bind('open', function(){
		if(!$(this).isVisible){
			$(this).show();
			
			$('body').css(
				'padding-top',
				parseInt($('body').css('padding-top'))+25+'px'
			);
		}
	});
	
	$('.create,.update,.close').hide();
	
	// Boutton d'enregistrement des nouvelles pages
	$('#wysiwyg-toolbar .create').click(function(){
		var element = $('.editable.focus');
		page = {
			internalTitle: prompt('Quel est le titre de cette nouvelle page ?'),
			html: element.ckeditorGet().getData()
		};
		
		$.post(
			$(this).attr('data-action'), {
				'internalTitle': page.internalTitle,
				'html': page.html,
				'csrf': $(this).attr('data-csrf')
			}, function(data){
				if(data.result == 1){
					console.log('La page a bien été créée.');
					$(location).attr('href', data.redirect);
				} else {
					console.log('Une erreur est survenue lors de la création de la page');
				}
			}, "json"
		);
		
		$('#wysiwyg-toolbar').trigger('close');
	});
	
	// Boutton d'enregistrement des pages existantes
	$('#wysiwyg-toolbar .update').click(function(){
		element = $('.editable.focus');
		page = {
			id : element.attr('data-id'),
			html : element.ckeditorGet().getData()
		};
		
		$.post(
			$(this).attr('data-action'), {
				'id' : page.id,
				'html' : page.html,
				'csrf' : $(this).attr('data-csrf')
			}, function(data){
				if(data.result == 1){
					console.log('La page #'+page.id+' a bien été modifié.');
				} else {
					console.log('Une erreur est survenue lors de la modification de la page #'+page.id+'.');
				}
			}, "json"
		);
		
		$('#wysiwyg-toolbar').trigger('close');
	});
	
	// Boutton de fermeture du wysiwyg
	$('#wysiwyg-toolbar .close').click(function(){
		$(this).trigger('close');
	});
});
