$(function(){
	var toolbars = {
		'page': [
            [ 'Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList', 'Indent'],
            [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
            [ 'Format', 'FontSize', 'TextColor' ],
            [ 'Link', 'Unlink', '-', 'Smiley', 'Image', 'oembed' ],
            [ 'Table' ],
            [ 'Source' ]
        ]
	};
	
	$('textarea[class^="wysiwyg-"]').each(function(){
		var type = $(this).attr('class').split('-')[1];
		
		CKEDITOR.replace(this, {
			contentsCss: '/min/css/main.css',
			filebrowserImageUploadUrl:	'/bundles/easytekecms/kcfinder/upload.php?type=images',
			toolbar: toolbars[type],
			height: 300,
			autoGrow_minHeight: 300,
			autoGrow_onStartup: true
		});
		
		$(this).removeAttr('required');
	});
});