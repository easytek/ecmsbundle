$(function(){
	// Auto confirm
	$('body > .container a[data-confirm]').each(function(){
		$(this).click(function(){
			return confirm($(this).attr('data-confirm'));
		});
	});
	
	// Notifications Twitter Bootstrap
	$(".alert-message").hide().fadeIn();
});