/* Edition frontend, factoriser la conf wyssiwyg avec celle de l'admin ? */

CKEDITOR.editorConfig = function( config ) {
	config.dialog_backgroundCoverColor = '#000';
};

CKEDITOR.disableAutoInline = true;

$(function(){
	$('.editable').each(function(){
		CKEDITOR.inline(this, {
			dialog_backgroundCoverColor: '#000',
			extraPlugins: 'restsave',
			toolbar: [
                [ 'Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList'],
                [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                [ 'Format', 'FontSize', 'TextColor' ],
                [ 'Link', 'Unlink', '-', 'Image', 'oembed' ],
                [ 'Source', 'restsave' ]
            ]
		});
	});
});