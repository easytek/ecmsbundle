﻿CKEDITOR.plugins.add('restsave', {
	init: function(editor) {
		var pluginName = 'restsave';

		// Register the command.
		var command = editor.addCommand(pluginName, CKEDITOR.plugins.restsave);

		// Register the toolbar button.
		editor.ui.addButton && editor.ui.addButton('restsave', {
			label: 'Enregistrer les modifications',
			command: pluginName,
			toolbar: 'document,50',
			icon: this.path + 'icon.png'
		});
	}
});

CKEDITOR.plugins.restsave = {
	exec: function(editor) {
		var domElement = $(editor.element.$);
		$.ajax({  
			url: domElement.data('rest-resource'),
			type: domElement.data('rest-method'),
			data: { 'html': domElement.html(), 'csrf': REST_CSRF_TOKEN }
		});
	},
	canUndo: false,
	readOnly: 1
};
