<?php

namespace Easytek\EcmsBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class ConfiguratorPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
		$definition = $container->getDefinition('ecms.configuration');
		
        foreach ($container->findTaggedServiceIds('ecms.configuration.configurable') as $id => $attributes) {
			// Permet d'instancier les services au moment d'un addService (qui ne sert à rien d'autre par la suite) et ainsi s'assurer que
			// le service ecms.configuration est instancié avant les services taggués, ceci afin d'éviter qu'ils ne complètent les objets trop tôt.
			$definition->addMethodCall('addService', array(new Reference($id)));
	    }
    }
}