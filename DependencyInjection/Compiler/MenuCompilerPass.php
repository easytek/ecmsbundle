<?php

namespace Easytek\EcmsBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class MenuCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
    	if (!$container->hasDefinition('ecms.menu')) {
    		return;
    	}
    	
		$definition = $container->getDefinition('ecms.menu');
		
		$taggedServices = $container->findTaggedServiceIds('ecms.menu.linkable');
		
		foreach ($taggedServices as $id => $attributes) {
			$definition->addMethodCall('addService', array(new Reference($id)));
	    }
    }
}


