<?php

namespace Easytek\EcmsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('easytek_ecms');

        $rootNode
            ->children()
                ->booleanNode('doctrine_cache_invalidator')->defaultFalse()->end()
                ->booleanNode('sonata_block')->defaultFalse()->end()
                ->end()
        ;

        return $treeBuilder;
    }
}
