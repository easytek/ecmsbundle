<?php

namespace Easytek\EcmsBundle\Block;

use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\HttpFoundation\Response;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;

class AdminBlock extends BaseBlockService
{
	public function getDefaultSettings()
	{
		return array(
			'title' => 'CMS'
		);
	}

	/**
	 * @param BlockInterface $block
	 * @param Response $response
	 */
	public function execute(BlockContextInterface $blockContext, Response $response = null)
	{
		$settings = array_merge($this->getDefaultSettings(), $blockContext->getSettings());
		
		return $this->renderResponse('EasytekEcmsBundle:Block:admin_block.html.twig', array(
			'settings' => $settings,
			'block' => $blockContext->getBlock(),
		));
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
	{
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
	{
	}
}