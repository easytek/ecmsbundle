<?php

namespace Easytek\EcmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * MenuSortable controller.
 * 
 * @Route("/menu")
 */
class MenuSortableController extends NestedSetSortableController
{
	private $repositoryName = 'EasytekEcmsBundle:MenuItem';
	private $maxLevel = 2; // À déplacer dans un fichier de configuration pour pouvoir y accéder depuis MenuController également.
	
	/**
	 * Déplace un élément du menu par rappart à un élement frère
	 *
	 * @Route("/item/{entityId}/moveAsNextSiblingOf/{siblingEntityId}", requirements={"entityId" = "\d+", "siblingEntityId" = "\d+"}, name="ecms_menu_moveAsNextSiblingOf")
	 * @Method("POST")
	 * @Template()
	 */
	public function moveAsNextSiblingOfAction($entityId, $siblingEntityId, $repositoryName = null, $maxLevel = null)
	{
		return parent::moveAsNextSiblingOfAction($entityId, $siblingEntityId, $this->repositoryName, $this->maxLevel);
	}
	
	/**
	 * Déplace un élément du menu par rapport à un élement parent
	 *
	 * @Route("/item/{entityId}/moveAsFirstChildOf/{parentEntityId}", requirements={"entityId" = "\d+", "parentEntityId" = "\d+"}, name="ecms_menu_moveAsFirstChildOf")
	 * @Method("POST")
	 * @Template()
	 */
	public function moveAsFirstChildOfAction($entityId, $parentEntityId, $repositoryName = null, $maxLevel = null)
	{
		return parent::moveAsFirstChildOfAction($entityId, $parentEntityId, $this->repositoryName, $this->maxLevel);
	}
}
