<?php

namespace Easytek\EcmsBundle\Controller;

use Easytek\EcmsBundle\Form\PageAdminType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Easytek\EcmsBundle\Entity\Page;
use Easytek\EcmsBundle\Form\PageType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageController extends Controller
{
    private $intention = 'r3$t'; // pour le CSRF

    /**
     * @Route("/page/", name="ecms_page_index")
     * @Template
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EasytekEcmsBundle:Page')->findAll();

        $deleteForms = array();

        foreach ($entities as $page) {
            $deleteForms[$page->getId()] = $this->createDeleteForm($page->getId())->createView();
        }

        return array(
            'entities' => $entities,
            'delete_forms' => $deleteForms,
            'liveEdit' => $this->get('ecms.configuration')->get('general', 'liveEdit'),
        );
    }

    /**
     * @Route(
     *        "/{_locale}/page/{id}-{slug}",
     *        name="ecms_i18n_page_show",
     *        requirements={"slug"="[a-zA-Z1-9\-_\/]*", "id"="^\d+$", "_locale"="en|fr"}
     * )
     * @Route(
     *        "/page/{id}-{slug}",
     *        name="ecms_page_show",
     *        requirements={"slug"="[a-zA-Z1-9\-_\/]*", "id"="^\d+$"}
     * )
     * @Template
     */
    public function showAction(Page $page, $slug = null)
    {
        $deleteForm = $this->createDeleteForm($page->getId());

        // Pour éviter le "duplicate content", on s'assure que le slug est le bon, sinon, redirection.
        if (!$page->isHomepage() && $slug != $page->getSlug()) {
// 			$route = 'ecms_page_show'
            $route = 'ecms_i18n_page_show';

            return $this->redirect(
                $this->generateUrl($route, array(
                    'id' => $page->getId(),
                    'slug' => $page->getSlug(),
                ))
            );
        }

        // Si on accède à la homepage via son url complète on redirige sur la version courte ("/" habituellement)
        if ($page->isHomepage() && $slug !== null) {
            return $this->redirect($this->generateUrl('ecms_homepage'));
            // return $this->redirect($this->generateUrl('ecms_i18n_homepage'));
        }

        $data = array(
            'page' => $page,
            'delete_form' => $deleteForm->createView(),
            'csrf' => $this->generateRestCsrfToken(),
            'liveEdit' => $this->get("ecms.configuration")->get('general', 'liveEdit'),
        );

        if ($page->getTemplate() != null) {
            $template = $page->getTemplate();

            if (!strpos($page->getTemplate(), '.')) {
                $template = 'EasytekEcmsBundle:Page:' . $template . '.html.twig';
            }

            return $this->render($template, $data);
        } else {
            return $data;
        }
    }

    /**
     * @Template
     */
    public function showAsBlockAction(Request $request, $id = null, $internalTitle = null, $i18n = false)
    {
        if ($id == null && $internalTitle == null) {
            throw new \Exception('No id or internalTitle provided.');
        }

        $locale = $i18n ? $request->getLocale() : null;
        $page = $this->getDoctrine()->getManager()->getRepository('EasytekEcmsBundle:Page')
            ->findOneByInternalTitle($internalTitle, $locale);

        if (!$page) {
            throw $this->createNotFoundException('Page introuvable (internalTitle = "' . $internalTitle . '", locale = "' . $locale . '").');
        }

        return array(
            'page' => $page
        );
    }

    /**
     * @Template("EasytekEcmsBundle:Page:show.html.twig")
     */
    public function homeAction(Request $request)
    {
        $locale = $request->get('_route') === 'ecms_i18n_homepage' ? $request->getLocale() : null;

        $page = $this->getDoctrine()->getManager()->getRepository('EasytekEcmsBundle:Page')->findHomepage($locale);

        if (!$page && $locale !== null) {
            $page = $this->getDoctrine()->getManager()->getRepository('EasytekEcmsBundle:Page')->findHomepage();
        }

        if (!$page) {
            throw $this->createNotFoundException('Homepage not found.');
        }

        return $this->showAction($page);
    }

    public function homeRedirectAction(Request $request)
    {
        $locale = $request->get('_route') === 'ecms_i18n_homepage' ? $request->getLocale() : null;

        $page = $this->getDoctrine()->getManager()->getRepository('EasytekEcmsBundle:Page')->findHomepage($locale);

        if (!$page && $locale !== null) {
            $page = $this->getDoctrine()->getManager()->getRepository('EasytekEcmsBundle:Page')->findHomepage();
        }

        if (!$page) {
            throw $this->createNotFoundException('Homepage not found.');
        }

        return $this->forward(
            'EasytekEcmsBundle:Page:show',
            array('id' => $page->getId()) // TODO : Optimiser ici pour ne pas faire 2 req
        );
    }

    /**
     * @Route("/page/new", name="ecms_page_new")
     * @Template
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function newAction()
    {
        $page = new Page();
        $form = $this->createPageForm($page);

        return array(
            'page' => $page,
            'form' => $form->createView(),
            'csrf' => $this->generateRestCsrfToken(),
        );
    }

    /**
     * Creates a new Page entity.
     *
     * @Route("/page/create", name="ecms_page_create")
     * @Method("post")
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function createAction()
    {
        $entity = new Page();
        $form = $this->createPageForm($entity);
        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'La page a bien été créée');

            return $this->redirect($this->generateUrl('ecms_page_index', array(
                'id' => $entity->getId(),
                'slug' => $entity->getSlug()
            )));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/pages", name="ecms_rest_page_create")
     * @Method("POST")
     */
    public function restCreateAction()
    {
        $this->securityChecks();

        // Génération du titre (temporaire, à remplacer par une autre solution)
        preg_match('/<h[12]{1}>([^<]*)<\/h[12]{1}>/', $this->getRequest()->get('html'), $match);
        $internalTitle = isset($match[1]) ? $match[1] : "Nouvelle page #" . uniqid();

        $page = new Page();
        $page
            ->setInternalTitle($internalTitle)
            ->setHtml($this->getRequest()->get('html'));

        // TODO : Rajouter une validation de l'objet

        $em = $this->getDoctrine()->getManager();
        $em->persist($page);
        $em->flush();

        return new Response();
    }

    /**
     * @Route("/page/{id}/edit", name="ecms_page_edit")
     * @Template
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function editAction($id)
    {
        $entity = $this->findPageOr404($id);
        $form = $this->createPageForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * @Route("/page/{id}/update", name="ecms_page_update")
     * @Method("post")
     * @Template("EasytekEcmsBundle:Page:edit.html.twig")
     * @Secure(roles="ROLE_CMS_ADMIN")
     */

    public function updateAction($id)
    {
        $entity = $this->findPageOr404($id);
        $oldPage = clone $entity;

        $form = $this->createForm(new PageType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Modification réussie !');

            $this->updateMenu($oldPage, $entity);

            return $this->redirect($this->generateUrl('ecms_page_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * REST Page Update (avec CSRF pour le moment)
     *
     * @Route("/pages/{id}", name="ecms_rest_page_update")
     * @Method("PUT")
     *
     * @throws NotFoundHttpException
     */
    public function restUpdateAction($id)
    {
        $this->securityChecks();

        $page = $this->findPageOr404($id);
// 		$oldPage = clone $page;
        // TODO : Rajouter tous les champs possibles
        $page->setHtml($this->getRequest()->get('html'));

        // TODO : Rajouter une validation de l'objet

        $em = $this->getDoctrine()->getManager();
        $em->persist($page);
        $em->flush();

// 		$this->updateMenu($oldPage, $page);

        return new Response();
    }

    private function updateMenu(Page $oldPage, Page $newPage)
    {
        $oldUri = serialize(array(
            'name' => 'ecms_page_show',
            'parameters' => array(
                'id' => $oldPage->getId(),
                'slug' => $oldPage->getSlug(),
            )
        ));

        $newUri = serialize(array(
            'name' => 'ecms_page_show',
            'parameters' => array(
                'id' => $newPage->getId(),
                'slug' => $newPage->getSlug(),
            )
        ));

        // TODO : Faire ça via un listener plutôt
        // Si le nouveau lien vers la page change à cause d'un changement de slug, il faut modifier les liens du menu.
        if ($newUri != $oldUri) {
            $menuItemRepo = $this->getDoctrine()->getRepository('EasytekEcmsBundle:MenuItem');
            $menuItems = $menuItemRepo->findBy(array('uri' => $oldUri));

            if ($menuItems != null) {
                $em = $this->getDoctrine()->getManager();

                foreach ($menuItems as $menuItem) {
                    $menuItem->setUri($newUri);
                    $em->persist($menuItem);
                }

                $em->flush();
            }
        }
    }

    /**
     * Deletes a Page entity.
     *
     * @Route("/page/{id}/delete", name="ecms_page_delete")
     * @Method("POST")
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bind($request);

        if ($form->isValid()) {
            $entity = $this->findPageOr404($id);

            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'La page a bien été supprimée');
        } else {
            // TODO: Gérer plus proprement les erreurs
            print_r($form->getErrors());
            exit;
        }

        return $this->redirect($this->generateUrl('ecms_page_index'));
    }

    /**
     * @Template
     */
    public function wysiwygToolbarAction()
    {
        return array(
            'csrf' => $this->generateCsrfTokenAjax(),
        );
    }

    private function findPageOr404($id, $exceptionMessage = null)
    {
        $entity = $this->getDoctrine()->getManager()->getRepository('EasytekEcmsBundle:Page')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($exceptionMessage != null ? $exceptionMessage : 'Page #' . $id . ' introuvable.');
        }

        return $entity;
    }

    public function createPageForm($data = null, array $options = array())
    {
        if ($this->get('security.context')->isGranted('ROLE_SUPERADMIN')) {
            $type = new PageAdminType();
        } else {
            $type = new PageType();
        }

        return parent::createForm($type, $data, $options);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))->add('id', 'hidden')->getForm();
    }

    private function generateRestCsrfToken()
    {
        return $this->get('form.csrf_provider')->generateCsrfToken($this->intention);
    }

    /**
     * @Route("/{page}.html", name="ecms_page_static")
     */
    public function staticAction($page)
    {
        $template = 'EasytekEcmsBundle:Page:static/' . $page . '.html.twig';

        if (!$this->get('templating')->exists($template)) {
            throw new NotFoundHttpException();
        }

        return $this->render($template);
    }

    private function securityChecks()
    {
        // TODO : Check droits admin

        if (!$this->isCsrfTokenValid($this->intention, $this->getRequest()->get('csrf'))) {
            throw new HttpException(403);
        }
    }
}
