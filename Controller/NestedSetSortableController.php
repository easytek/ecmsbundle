<?php

namespace Easytek\EcmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;

/**
 * NestedSetSortable controller.
 */
class NestedSetSortableController extends Controller
{
	private $intention = 'ajaxNestedSortable'; // pour le CSRF
	
	private function generateCsrfTokenAjax()
	{
		return $this->get('form.csrf_provider')->generateCsrfToken($this->intention);
	}
	
	public function getCSRFTokenAction()
	{
		return new Response($this->generateCsrfTokenAjax());
	}
	
	private function securityChecks()
	{
// 		if (!$this->isCsrfTokenValid($this->intention, $this->getRequest()->get('csrf'))) {
// 			throw new HttpException(403);
//			return new Response(null, 403);
// 		}
	}
	
	/**
	 * Déplace une entité par rappart à une entité soeur.
	 */
	public function moveAsNextSiblingOfAction($entityId, $siblingEntityId, $repositoryName, $maxLevel = null)
	{
		$this->securityChecks();
		
		$repository = $this->getDoctrine()->getRepository($repositoryName);
		$entity = $repository->find($entityId);
		$siblingEntity = $repository->find($siblingEntityId);
		
		if (!$entity || !$siblingEntity) {
			return new HttpException("L'entité déplacé ou l'entité frère introuvable.", 500);
		}
		
		if ($maxLevel != null && $entity->getMaxChildrenLevelIfMovedAsNextSiblingOfAction($siblingEntity) > $maxLevel) {
			return new HttpException("Niveau maximum (".$maxLevel.") dépassé par le déplacement.", 500);
		}
		
		$repository->persistAsNextSiblingOf($entity, $siblingEntity);
		$this->getDoctrine()->getManager()->flush();
		
		return new Response();
	}
	
	/**
	 * Déplace une entité par rapport à une entité parent.
	 */
	public function moveAsFirstChildOfAction($entityId, $parentEntityId, $repositoryName, $maxLevel = null)
	{
		$this->securityChecks();
		
		$repository = $this->getDoctrine()->getRepository($repositoryName);
		$entity = $repository->find($entityId);
		$parentEntity = $repository->find($parentEntityId);
		
		if (!$entity || !$parentEntity) {
			return new HttpException("Entité déplacé ou entité parent introuvable.", 500);
		}
		
		if ($maxLevel != null && $entity->getMaxChildrenLevelIfMovedAsFirstChildOf($parentEntity) > $maxLevel) {
			return new HttpException("Niveau maximum (".$maxLevel.") dépassé par le déplacement.", 500);
		}
		
		$repository->persistAsFirstChildOf($entity, $parentEntity);
		$this->getDoctrine()->getManager()->flush();
		
		return new Response();
	}
}
