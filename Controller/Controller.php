<?php

namespace Easytek\EcmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;


class Controller extends BaseController
{
    /**
     * TODO : supprimer cette méthode lorsque tous les projets utilisant ce bundle auront migré en 2.6
     *
     * Checks the validity of a CSRF token.
     *
     * @param string $id    The id used when generating the token
     * @param string $token The actual token sent with the request that should be validated
     *
     * @return bool
     */
    protected function isCsrfTokenValid($id, $token)
    {
        return $this->get('form.csrf_provider')->isCsrfTokenValid($id, $token);
    }
}
