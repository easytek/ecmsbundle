<?php

namespace Easytek\EcmsBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Easytek\EcmsBundle\Entity\MenuItem;
use Easytek\EcmsBundle\Repository\MenuItemRepository;
use Easytek\EcmsBundle\Form\Type\MenuItemType;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Easytek\EcmsBundle\Form\Type\RootMenuItemType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/menu")
 */
class MenuController extends Controller
{
    private $intention = '!menu!'; // pour le CSRF

    /**
     * @Route("/", name="cms_menu_index")
     * @Template()
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function indexAction()
    {
        $rootNodes = $this->getDoctrine()->getRepository('EasytekEcmsBundle:MenuItem')->getRootNodes();

        return array(
            'rootNodes' => $rootNodes,
        );
    }

    /**
     * @Route("/{rootName}/show", name="cms_menu_show")
     * @Template()
     */
    public function showAction($rootName)
    {
        $menu = $this->getDoctrine()->getRepository('EasytekEcmsBundle:MenuItem')->getMenuByRootName($rootName);

        return array(
            'menu' => $menu,
            'csrf' => $this->generateCsrfToken(),
        );
    }

    /**
     * @Route("/{rootId}/edit", name="cms_menu_edit")
     * @Template()
     */
    public function editAction($rootId)
    {
        $menu = $this->getDoctrine()->getRepository('EasytekEcmsBundle:MenuItem')->getMenuByRootId($rootId, false);

        return array(
            'menu' => $menu,
            'csrf' => $this->generateCsrfToken(),
        );
    }

    /**
     * @Route("/new", name="cms_menu_new")
     * @Template()
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function newAction()
    {
        $form = $this->createForm(new RootMenuItemType(), new MenuItem());

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/create", name="cms_menu_create")
     * @Method("post")
     * @Template("EasytekEcmsBundle:Menu:new.html.twig")
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function createAction()
    {
        $rootMenuItem = new MenuItem();
        $form = $this->createForm(new RootMenuItemType(), $rootMenuItem);
        $form->bind($this->getRequest());

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($rootMenuItem);
            $em->flush();

            if (!$this->getRequest()->isXmlHttpRequest()) {
                return $this->redirect($this->generateUrl(
                    'cms_menu_edit',
                    array('rootId' => $rootMenuItem->getRoot()))
                );
            } else {
                return new Response();
            }
        }

        if (!$this->getRequest()->isXmlHttpRequest()) {
            return array(
                'form' => $form->createView(),
            );
        } else {
            return new Response();
        }
    }

    /**
     * @Route("/item/{parentId}/newChild", name="cms_menu_item_new")
     * @Template()
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function newItemAction($parentId)
    {
        $entity = new MenuItem();
        $form = $this->createMenuItemForm($entity);

        $repo = $this->getDoctrine()->getRepository('EasytekEcmsBundle:MenuItem');
        $parentEntity = $repo->find($parentId);

        if (!$parentEntity) {
            throw $this->createNotFoundException('Unable to find MenuItem parent entity.');
        }

        return array(
            'entity' => $entity,
            'parentEntity' => $parentEntity,
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/create/{parentId}", name="cms_menu_item_create")
     * @Method("post")
     * @Template("EasytekEcmsBundle:Menu:newItem.html.twig")
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function createItemAction(Request $request, $parentId)
    {
        $parentEntity = $this->findMenuItemOr404($parentId,
            "L'élément de menu parent #" . $parentId . " est introuvable.");

        $menuItem = new MenuItem();
        $form = $this->createMenuItemForm($menuItem);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->getDoctrine()->getRepository('EasytekEcmsBundle:MenuItem')->persistAsLastChildOf($menuItem,
                $parentEntity);
            $this->getDoctrine()->getManager()->flush();

            if (!$this->getRequest()->isXmlHttpRequest()) {
                return $this->redirect($this->generateUrl('cms_menu_edit', array('rootId' => $menuItem->getRoot())));
            } else {
                return new Response();
            }
        }

        $data = array(
            'entity' => $menuItem,
            'parentEntity' => $parentEntity,
            'form' => $form->createView(),
        );

        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $data;
        } else {
            return new Response(json_encode(array(
                'result' => 0,
                'html' => $this->renderView('EasytekEcmsBundle:Menu:newDialog.html.twig', $data)
            )));
        }
    }

    /**
     * @param MenuItem $menuItem
     * @return Form
     */
    private function createMenuItemForm(MenuItem $menuItem)
    {
        $uriChoice = $this->get('ecms.menu')->getUriChoice();

        $form = $this->createForm(new MenuItemType($uriChoice), $menuItem);

        return $form;
    }

    /**
     * Displays a form to edit an existing MenuItem entity.
     *
     * @Route("/item/{id}/edit", name="cms_menu_item_edit")
     * @Template()
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function editItemAction($id)
    {
        $menuItem = $this->findMenuItemOr404($id);
        $form = $this->createMenuItemForm($menuItem);

        return array(
            'menuItem' => $menuItem,
            'form' => $form->createView(),
        );
    }

    /**
     * Edits an existing MenuItem entity.
     *
     * @Route("/item/{id}/update", name="cms_menu_item_update")
     * @Method("post")
     * @Template("EasytekEcmsBundle:Menu:editItem.html.twig")
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function updateItemAction(Request $request, $id)
    {
        $menuItem = $this->findMenuItemOr404($id);

        $form = $this->createMenuItemForm($menuItem);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $parentEntity = $menuItem->getParent();

            $em = $this->getDoctrine()->getManager();
            $em->persist($menuItem);
            $em->flush();

            if (!$this->getRequest()->isXmlHttpRequest()) {
                return $this->redirect($this->generateUrl('cms_menu_edit', array('rootId' => $menuItem->getRoot())));
            } else {
                return new Response();
            }
        }

        if (!$this->getRequest()->isXmlHttpRequest()) {
            return array(
                'menuItem' => $menuItem,
                'form' => $form->createView(),
            );
        } else {
            return new Response();
        }
    }

    /**
     * Deletes a MenuItem entity.
     *
     * @Route("/{id}/delete/{csrf}", name="cms_menu_item_delete")
     * @Method("get")
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function deleteItemAction($csrf, $id)
    {
        $this->isCsrfTokenValidOr403($csrf);

        $menuItem = $this->findMenuItemOr404($id);

        $parentEntity = $menuItem->getParent();

        $em = $this->getDoctrine()->getManager();
        $em->remove($menuItem);
        $em->flush();

        if (!$this->getRequest()->isXmlHttpRequest()) {
            return $this->redirect($this->generateUrl('cms_menu_edit', array('rootId' => $menuItem->getRoot())));
        } else {
            return new Response();
        }
    }

// 	public function getCSRFTokenAction()
// 	{
// 		return new Response($this->generateCsrfToken());
// 	}

    private function generateCsrfToken()
    {
        return $this->get('form.csrf_provider')->generateCsrfToken($this->intention);
    }

    private function isCsrfTokenValidOr403($csrf, $message = 'Accès non autorisé.')
    {
        if (!$this->isCsrfTokenValid($this->intention, $csrf)) {
            throw new AccessDeniedException($message);
        }
    }

    /**
     * @Route("/createFixture", name="cms_menu_createFixture")
     * @Template()
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function createFixtureAction()
    {
        $em = $this->getDoctrine()->getManager();

        $itemRoot = new MenuItem();
        $itemRoot->setName('menu1');

        $em->persist($itemRoot);

        for ($i = 1; $i <= 2; $i++) {
            $item = new MenuItem();
            $item->setName('element ' . $i)->setUri('/element' . $i);
            $itemRoot->addChild($item);
            $em->persist($item);

// 			for ($y = 1; $y <= 5; $y++)
// 			{
// 				$sitem = new MenuItem();
// 				$sitem->setName('sous-element ' . $y)->setUri('/sous-element-' . $y);
// 				$item->addChild($sitem);
// 				$em->persist($sitem);

// 				for ($z = 1; $z <= 3; $z++)
// 				{
// 					$ssitem = new MenuItem();
// 					$ssitem->setName('sous-sous-element ' . $z)->setUri('/sous-sous-element-' . $z);
// 					$sitem->addChild($ssitem);
// 					$em->persist($ssitem);
// 				}
// 			}
        }

        $em->flush();

        return new Response('ok');
    }

    /**
     * @param int $id
     * @return MenuItem
     */
    private function findMenuItemOr404($id, $exceptionMessage = null)
    {
        $entity = $this->getDoctrine()->getRepository('EasytekEcmsBundle:MenuItem')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException($exceptionMessage == null ? sprintf("L'élément de menu #%d est introuvable.",
                $id) : $exceptionMessage);
        }

        return $entity;
    }

    /**
     * @Route("/getAdminDialogs", name="cms_menu_getAdminDialogs")
     * @Template()
     */
    public function adminDialogsAction()
    {
        $menuItems = $this->getDoctrine()->getRepository('EasytekEcmsBundle:MenuItem')->getMenu();

        $editForms = array();

        foreach ($menuItems as $menuItem) {
            $editForms[$menuItem->getId()] = $this->createMenuItemForm($menuItem)->createView();
        }

        $maxLevel = 1;
        $newMenuItem = new MenuItem();
        $newForms = array();

        foreach ($menuItems as $menuItem) {
            if ($menuItem->getLvl() <= $maxLevel) // Lvl écrit en dur mais il faudrait remplacer par un lecture d'un fichier de configuration
            {
                $newForms[$menuItem->getId()] = $this->createMenuItemForm($newMenuItem)->createView();
            }
        }

        return array(
            'menuItems' => $menuItems,
            'editForms' => $editForms,
            'newForms' => $newForms,
            'csrf' => $this->generateCsrfToken(),
            'maxLevel' => $maxLevel,
        );
    }

    /**
     * @Route("/{csrf}/{id}", name="cms_menu_rest_delete")
     * @Method("DELETE")
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
    public function deleteRestAction($csrf, $id)
    {
        if (!$this->isCsrfTokenValid($this->intention, $csrf)) {
            throw $this->createNotFoundException('csrf invalid');
        }

// 		$this->isCsrfTokenValidOr404($this->intention, $csrf);

        $entity = $this->findMenuItemOr404($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        return new Response(json_encode(array('result' => 1)));
    }

    /**
     * @Template
     */
    public function adminMenuAction()
    {
        $adminMenu = $this->get('ecms.menu')->getAdminMenu();

        return array('adminMenu' => $adminMenu);
    }
}
