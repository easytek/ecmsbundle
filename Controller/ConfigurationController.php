<?php

namespace Easytek\EcmsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Easytek\EcmsBundle\Service\Configuration;
use Easytek\EcmsBundle\Entity\ConfigurationItem;
use Easytek\EcmsBundle\Form\Configuration\ConfigurationType;
use Easytek\EcmsBundle\Form\Configuration\ConfigurationItemType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Assetic\Exception\Exception;

/**
 * @Route("/configuration")
 */
class ConfigurationController extends Controller
{
    /**
     * Lists all services configuration as a form
     *
     * @Route("/", name="ecms_configuration")
     * @Template()
     * @Secure(roles="ROLE_CMS_ADMIN")
     */
	public function indexAction(Request $request)
	{
		$configuration = $this->get('ecms.configuration');
		
		$form = $this->createForm(new ConfigurationType(), $configuration);
		
		return array(
			'form' => $form->createView()
		);
	}
	
	/**
	 * Process the posted form
	 *
	 * @Route("/update", name="ecms_configuration_update")
	 * @Method("POST")
	 * @Template("EasytekEcmsBundle:Configuration:index.html.twig")
	 * @Secure(roles="ROLE_CMS_ADMIN")
	 */
	public function updateAction()
	{
		$configuration = $this->get('ecms.configuration');
		
		$form = $this->createForm(new ConfigurationType(), $configuration);
		
		$form->bind($this->getRequest());
		
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
	
			foreach ($configuration->getConfigurationItems() as $configurationItem) {
				$em->persist($configurationItem);
			}
	
			$em->flush();
			
			$this->get('session')->getFlashBag()->add('success', 'La configuration a bien été modifiée.');
			
			return $this->redirect($this->generateUrl('ecms_configuration'));
		}
		
		return array(
			'form' => $form->createView()
		);
	}
	
}
