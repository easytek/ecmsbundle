<?php

namespace Easytek\EcmsBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PageControllerTest extends WebTestCase
{
    
    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient();
        
        // Create a new entry in the database
        $crawler = $client->request('GET', '/page/');
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        $crawler = $client->click($crawler->selectLink('Créer une nouvelle page')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Créer')->form(array(
            'easytek_ecmsbundle_pagetype[internalTitle]'  => 'Nouvelle page',
        	'easytek_ecmsbundle_pagetype[html]'  => 'Du texte dans la page',
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertTrue($crawler->filter('div[class="page"]:contains("Du texte dans la page")')->count() > 0);

         // Retourner sur la page d'accueil
        $crawler = $client->request('GET', '/page/');
        
        // On vérifie que la nouvelle page est bien listée
        $this->assertTrue($crawler->filter('td:contains("Nouvelle page")')->count() > 0);
        
        //On vérifie qu'on accède bien à la page créée
        $crawler = $client->click($crawler->filter('td:contains("Nouvelle page")')->siblings()->eq(1)->filter('a:contains("afficher")')->eq(0)->link());
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
        
        // Retourner sur la page d'accueil
        $crawler = $client->request('GET', '/page/');
        
        // Edit the entity
        $crawler = $client->click($crawler->filter('td:contains("Nouvelle page")')->siblings()->eq(1)->filter('a:contains("modifier")')->eq(0)->link());

        $form = $crawler->selectButton('Enregistrer')->form(array(
            'easytek_ecmsbundle_pagetype[internalTitle]'  => 'Nouvelle page modifiée',
            'easytek_ecmsbundle_pagetype[html]'  => 'Du texte modifié dans la page',
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();
        
        // Retourner sur la page d'accueil
        $crawler = $client->request('GET', '/page/');
        
        // On sauvegarde le lien de la page pour le tester plus tard après la suppresion
        $lien_afficher_page = $crawler->filter('td:contains("Nouvelle page modifiée")')->siblings()->eq(1)->filter('a:contains("afficher")')->eq(0)->link();

        // Check data in the show view
        $this->assertTrue($crawler->filter('td:contains("Nouvelle page modifiée")')->count() > 0);
        
        // Suppresion de la page
        $js = $crawler->filter('td:contains("Nouvelle page modifiée")')->siblings()->eq(1)->filter('a:contains("supprimer")')->eq(0)->attr('onclick');
        preg_match('/[a-z0-9]{40}/', $js, $match);
        $token = $match[0];
        
        preg_match('/value.+([0-9])\'/', $js, $match);
        $pageId = $match[1];
        
        $href = $crawler->filter('td:contains("Nouvelle page modifiée")')->siblings()->eq(1)->filter('a:contains("supprimer")')->eq(0)->attr('href');
        
        $client->request(
        		'POST',
        		$href,
        		array(
        				'form' => array(
        						'_token' => $token,
        						'id' => $pageId,
        				)
        		)
        );
        
        $this->assertTrue(302 === $client->getResponse()->getStatusCode());
        
        $crawler = $client->followRedirect();
        
        // Vérifie si la page a bien été supprimée
        $this->assertTrue($crawler->filter('td:contains("Nouvelle page modifiée")')->count() == 0);
        
        // Et que l'on ne peut plus accéder à la page
        $crawler = $client->click($lien_afficher_page );
        $this->assertTrue(404 === $client->getResponse()->getStatusCode());

        // Check the entity has been delete on the list
//         $this->assertNotRegExp('/Foo/', $client->getResponse()->getContent());
    }
   
}
