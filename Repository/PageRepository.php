<?php

namespace Easytek\EcmsBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PageRepository extends EntityRepository
{
	public function findAll()
	{
		return $this->getEntityManager()
			->createQuery('SELECT p FROM EasytekEcmsBundle:Page p ORDER BY p.internalTitle ASC')
			->getResult()
		;
	}
	
	public function find($id, $cache = true)
	{
		return $this->createQueryBuilder('p')
			->where('p.id = :id')
			->orderBy('p.internalTitle')
			->setParameter(':id', $id)
			->getQuery()
				->useResultCache($cache, null, 'page_'.$id)
				->getOneOrNullResult()
		;
	}
	
	public function findHomepage($locale = null, $cache = true)
	{
		$qb = $this->createQueryBuilder('p')
			->where('p.homepage = :homepage')
			->setMaxResults(1)
			->setParameter('homepage', true)
		;
		
		if ($locale !== null) {
			$qb
				->andWhere('p.locale = :locale')
				->setParameter('locale', $locale)
			;
		}
		
		return $qb->getQuery()
			->useResultCache($cache, null, 'page_home')
			->getOneOrNullResult()
		;
	}
	
	public function findOneBySlug($slug, $locale = null, $cache = true)
	{
		$qb = $this->createQueryBuilder('p')
			->where('p.slug = :slug')
			->setParameter('slug', $slug)
		;
		
		if ($locale !== null) {
			$qb
				->andWhere('p.locale = :locale')
				->setParameter('locale', $locale)
			;
		}

		return $qb->getQuery()
			->useResultCache($cache, null, 'page_by_slug_' . $slug)
			->getOneOrNullResult()
		;
	}

    public function findOneByInternalTitle($internalTitle, $locale = null, $cache = true)
    {
        $qb = $this->createQueryBuilder('p')
            ->where('p.internalTitle = :internalTitle')
            ->setParameter('internalTitle', $internalTitle)
        ;

        if ($locale !== null) {
            $qb
                ->andWhere('p.locale = :locale')
                ->setParameter('locale', $locale)
            ;
        }

        return $qb->getQuery()
            ->useResultCache($cache, null, 'page_by_internalTitle_' . $internalTitle)
            ->getOneOrNullResult()
        ;
    }
}
