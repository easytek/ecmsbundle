<?php

namespace Easytek\EcmsBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

class ConfigurationItemRepository extends EntityRepository
{
	public function findAll($cache = true)
	{
		return $this->createQueryBuilder('c')->orderBy('c.bundleName')->getQuery()
			->useResultCache($cache, null, 'configuration')
			->getResult();
		;
	}
}