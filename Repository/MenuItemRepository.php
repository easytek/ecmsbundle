<?php

namespace Easytek\EcmsBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Common\Collections\ArrayCollection;

class MenuItemRepository extends NestedTreeRepository
{
	/**
	 * Stores already fetched menus
	 */
	private $cache = array();
	

	public function retrieveMenuByRootId($rootId, $cache = true)
	{
		$qb = $this->getEntityManager()->createQueryBuilder();
		$qb
			->select('node, c, p')
			->from('Easytek\EcmsBundle\Entity\MenuItem', 'node')
			->leftJoin('node.parent', 'p')
			->leftJoin('node.children', 'c')
			->orderBy('node.root, node.lft')
			->where('node.root = :rootId')
			->setParameter('rootId', $rootId)
		;
		
		return $qb->getQuery()
			->useResultCache($cache, null, sprintf('menu_by_root_id_%s', $rootId))
			->getResult()
		;
	}
	
	public function retrieveMenuByRootName($rootName, $cache = true)
	{
		// TODO : Optimiser en une requete imbriquée (et fusionner les 2 methodes de cache)
		$qb = $this->createQueryBuilder('node');
		$qb
			->where($qb->expr()->isNull('node.parent'))
			->andWhere('node.name = :rootName')
			->setParameter(':rootName', $rootName)
		;
		
		$rootNode = $qb->getQuery()
			->useResultCache($cache, null, sprintf('menu_by_root_name_%s', $rootName))
			->getOneOrNullResult()
		;
		
		if ($rootNode != null) {
			return $this->retrieveMenuByRootId($rootNode->getId());
		} else {
			return null;
		}
	}
	
	public function getMenuByRootId($rootId, $cache = true)
	{
		if (!$cache || !isset($this->menu[$rootId])) {
			$this->cache[$rootId] = $this->retrieveMenuByRootId($rootId, $cache);
		}
		
		return $this->cache[$rootId];
	}
	
	public function getMenuByRootName($rootName, $cache = true)
	{
		if (!$cache || !isset($this->menu[$rootName])) {
			$this->cache[$rootName] = $this->retrieveMenuByRootName($rootName, $cache);
		}
	
		return $this->cache[$rootName];
	}
}