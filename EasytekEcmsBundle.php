<?php

namespace Easytek\EcmsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Easytek\EcmsBundle\DependencyInjection\Compiler\ConfiguratorPass;
use Easytek\EcmsBundle\DependencyInjection\Compiler\MenuCompilerPass;

class EasytekEcmsBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ConfiguratorPass());
		$container->addCompilerPass(new MenuCompilerPass());
    }
}