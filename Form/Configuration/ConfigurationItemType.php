<?php

namespace Easytek\EcmsBundle\Form\Configuration;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Easytek\EcmsBundle\Form\EventListener\SetLabelFieldSubscriber;

class ConfigurationItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $subscriber = new SetLabelFieldSubscriber($builder->getFormFactory());
        $builder->addEventSubscriber($subscriber);
    }
	
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Easytek\EcmsBundle\Entity\ConfigurationItem',
        ));
    }

    public function getName()
    {
        return 'easytek_ecmsbundle_configurationitem';
    }
}
