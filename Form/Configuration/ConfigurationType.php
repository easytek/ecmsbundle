<?php

namespace Easytek\EcmsBundle\Form\Configuration;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ConfigurationType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
		$builder->add('configurationItems', 'collection', array('type' => new ConfigurationItemType()));
    }
	
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Easytek\EcmsBundle\Service\Configuration',
        ));
    }
	
    public function getName()
    {
        return 'easytek_ecmsbundle_configuration';
    }
}
