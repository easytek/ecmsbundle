<?php

namespace Easytek\EcmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class PageAdminType extends PageType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
    	parent::buildForm($builder, $options);
    	
        $builder
            ->add('template', null, array(
				'attr' => array(
					'placeholder' => 'Nom du template spécifique',
				)
			))
        ;
    }

    public function getName()
    {
        return 'easytek_ecmsbundle_pageadmintype';
    }
}
