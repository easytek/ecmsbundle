<?php

namespace Easytek\EcmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PageType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
        $builder
            ->add('internalTitle', null, array(
				'attr' => array(
					'placeholder' => 'Titre interne de la page',
				)
			))
			// CKEditor n'est plus utilisé directement via le bundle, celui-ci ne permettant pas assez de configuration.
            ->add('html', 'textarea', array(
				'attr' => array(
					'class' => 'wysiwyg-page',
				)
			))
			->add('locale', 'choice', array(
				'label' => 'Langue',
				// TODO : définir la liste des langues en fonction de la conf
				'choices' => array( 
					'fr' => 'language.fr',
					'en' => 'language.en',
				) 
			))
            ->add('title', null, array(
                'label' => 'Titre de la fenêtre'
            ))
            ->add('description', null, array(
                'label' => 'Meta description'
            ))
            ->add('homepage', null, array(
                'label' => 'Marquer comme page d\'accueil'
            ))
        ;
	}

    public function getName()
    {
        return 'easytek_ecmsbundle_pagetype';
    }
}
