<?php

namespace Easytek\EcmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MenuItemType extends AbstractType
{
    private $uriChoices = array();

    public function __construct($uriChoices = array())
	{
		if (!empty($uriChoices)) {
			$this->uriChoices = $uriChoices;
		}
	}
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$builder->add('name', null, array(
    		'label' => 'Nom du lien'
    	));
    	
    	if (!empty($this->uriChoices)) {
    		$builder->add('uri', new ChoiceOrTextType(), array(
    			'choices'  => $this->uriChoices,
    			'choice_label' => 'Resources internes',
    			'text_label' => 'URL ou route',
    			'label' => false,
//     			'required' => false
    		));
    	}
    	
    	$builder->add('role', 'choice', array(
    		'label' => 'Visible pour',
    		'required' => false,
    		'choices' => array(
    			'' => 'Tout le monde',
    			'ROLE_ADMIN' => 'Administrateurs',
    			'ROLE_USER' => 'Utilisateurs',
    	)));
    }
    
    public function getName()
    {
        return 'easytek_ecmsbundle_menuitemtype';
    }
}
