<?php

namespace Easytek\EcmsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Easytek\EcmsBundle\Form\DataTransformer\ValueToChoiceOrTextTransformer;

class ChoiceOrTextType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('choice', 'choice', array(
                'choices' => $options['choices'],
                'required' => false,
            	'label' => $options['choice_label'],
            ))
            ->add('text', 'text', array(
                'required' => false,
            	'label' => $options['text_label'],
            ))
            ->addModelTransformer(new ValueToChoiceOrTextTransformer($options['choices']))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array('choices', 'choice_label', 'text_label'));
        $resolver->setAllowedTypes(array('choices' => 'array'));
    }
    
    public function getName()
    {
    	return 'choice_or_text';
    }
}
