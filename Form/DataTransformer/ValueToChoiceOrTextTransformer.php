<?php

namespace Easytek\EcmsBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class ValueToChoiceOrTextTransformer implements DataTransformerInterface
{
    private $choices;

    public function __construct(array $choices)
    {
        $this->choices = $choices;
    }

    public function transform($data)
    {
     	if ($data !== null && $data[0] === '{') { // Si $data est du json alors il vient de la liste
     		return array('choice' => $data, 'text' => null);
     	} else { // Sinon il vient du champ text
     		return array('choice' => null, 'text' => $data);
     	}
    }

    public function reverseTransform($data)
    {
    	if ($data['choice'] !== null) {
    		return $data['choice'];
    	} else {
    		return $data['text'];
    	}    	
    }
}