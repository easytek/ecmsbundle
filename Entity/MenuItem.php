<?php

namespace Easytek\EcmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Easytek\EcmsBundle\Entity\MenuItem;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Assetic\Exception\Exception;

/**
 * @ORM\Entity(repositoryClass="Easytek\EcmsBundle\Repository\MenuItemRepository")
 * @ORM\Table(name="ecms_menu_item")
 * @Gedmo\Tree(type="nested")
 */
class MenuItem extends NestedSetNode
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $uri
     *
     * @ORM\Column(name="uri", type="text", nullable=true)
     * @Assert\NotBlank
     * @Assert\Blank(groups={"rootMenuItem"})
     */
    private $uri;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=100)
     * @Assert\NotBlank
     */
    private $name;
    
    /**
     * @ORM\Column(name="role", type="string", length=50, nullable=true)
     * @var string
     */
    private $role;

    /**
     * @ORM\Column(name="lft", type="integer")
     * @Gedmo\TreeLeft
     */
    private $lft;

    /**
     * @ORM\Column(name="lvl", type="integer")
     * @Gedmo\TreeLevel
     */
    private $lvl;

    /**
     * @ORM\Column(name="rgt", type="integer")
     * @Gedmo\TreeRight
     */
    private $rgt;

    /**
     * @ORM\Column(name="root", type="integer", nullable=true)
     * @Gedmo\TreeRoot
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="MenuItem", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="MenuItem", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    
    public function __construct()
    {
    	$this->children = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uri
     *
     * @param string $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
		return $this;
    }

    /**
     * Get uri
     *
     * @return string 
     */
    public function getUri()
    {
        return $this->uri;
    }
    
    public function getRoute()
    {
    	if (!$this->hasRoute()) {
    		throw new \Exception('This MenuItem entity does not contain a route uri.');
    	}
    	
    	if ($this->hasJsonRoute()) {
    		return (array) json_decode($this->getUri());
    	} elseif ($this->hasRawRoute()) {
    		return array(
    			'name' => $this->getUri(),
    			'parameters' => array()
    		);
    	}
    	
    	throw new \Exception('Shouldn\'t happen :p');
    }
    
    private function hasJsonRoute()
    {
    	return preg_match('/^{/', $this->getUri());
    }
    
    private function hasRawRoute()
    {
    	return !$this->hasJsonRoute() && !preg_match('/(\/|#)/', $this->getUri());
    }
    
    public function hasRoute()
    {
    	return $this->hasJsonRoute() || $this->hasRawRoute();
    }
    
    public function getRouteName()
    {
    	$route = $this->getRoute();
   		return $route['name'];
    }
    
    public function getRouteParameters()
    {
      	$route = $this->getRoute();
   		return (array) $route['parameters'];
    }
    
    public function getHref($environment = 'prod')
    {
    	if ($environment != 'prod' && !preg_match('/^http/', $this->getUri())) {
    		return '/app_'.$environment.'.php'.$this->getUri();
    	} else {
    		return $this->getUri();
    	}
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
		return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
    	$this->role = $role;
    	return $this;
    }
    
    /**
     * @return string
     */
    public function getRole()
    {
    	return $this->role;
    }
    
   /**
    * Set lft
    *
    * @param integer $lft
    */
   public function setLft($lft)
   {
       $this->lft = $lft;
   }

   /**
    * Get lft
    *
    * @return integer 
    */
   public function getLft()
   {
       return $this->lft;
   }

   /**
    * Set lvl
    *
    * @param integer $lvl
    */
   public function setLvl($lvl)
   {
       $this->lvl = $lvl;
   }

   /**
    * Get lvl
    *
    * @return integer 
    */
   public function getLvl()
   {
       return $this->lvl;
   }

   /**
    * Set rgt
    *
    * @param integer $rgt
    */
   public function setRgt($rgt)
   {
       $this->rgt = $rgt;
   }

   /**
    * Get rgt
    *
    * @return integer 
    */
   public function getRgt()
   {
       return $this->rgt;
   }

   /**
    * Set root
    *
    * @param integer $root
    */
   public function setRoot($root)
   {
       $this->root = $root;
   }

   /**
    * Get root
    *
    * @return integer 
    */
   public function getRoot()
   {
       return $this->root;
   }

    /**
     * Set parent
     *
     * @param Easytek\EcmsBundle\Entity\MenuItem $parent
     */
    public function setParent(MenuItem $parent)
    {
        $this->parent = $parent;
    }

    /**
     * Get parent
     *
     * @return Easytek\EcmsBundle\Entity\MenuItem 
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    public function getParentId()
    {
    	if ($this->getParent() !== null) {
    		return $this->getParent()->getId();
    	} else {
    		return null;
    	}
    }
    
    public function getParentName()
    {
        if ($this->getParent() !== null) {
    		return $this->getParent()->getName();
    	} else {
    		return null;
    	}
    }

    /**
     * Add children
     *
     * @param Easytek\EcmsBundle\Entity\MenuItem $child
     */
    public function addChild(MenuItem $child)
    {
        $this->children[] = $child;
		$child->setParent($this);
    }

    /**
     * Get children
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getChildren()
    {
        return $this->children;
    }
	
	public function getOptions()
	{
		return array('uri' => $this->getUri());
	}
	
	public function __toString()
	{
		return $this->getName();
	}
	
	public function toArray()
	{
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'uri' => $this->getUri(),
		);
	}
}