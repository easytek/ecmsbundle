<?php

namespace Easytek\EcmsBundle\Entity;

use Knp\Menu\NodeInterface;

abstract class NestedSetNode
{
	/**
	 * Vérifie si l'élément a des enfants ou non.
	 *
	 * @return boolean
	 */
	public function hasChildren()
	{
		return (count($this->getChildren()) > 0);
	}
	
	/**
	 * Retourne le level du plus petit des enfants.
	 * @param int $biggestLevel ???
	 * @return int
	 */
	public function getMaxChildrenLevel($biggestLevel = 0)
	{
		$biggestLevel = ($biggestLevel > $this->getLvl()) ? $biggestLevel : $this->getLvl();
	
		if (!$this->hasChildren())
			return $biggestLevel;
		else
		{
			foreach ($this->getChildren() as $child)
				$biggestLevel = $child->getMaxChildrenLevel($biggestLevel);

			return $biggestLevel;
		}
	}
	
	/**
	 * Retourne le niveau du plus petit des enfants si l'élément était déplacé via un moveAsFirstChildOf.
	 * Utile pour s'assurer que le déplacement d'un élément n'a pas pour 
	 * conséquence de dépasser une éventuelle limite de profondeur de l'arbre.
	 * 
	 * @param object $parentEntity Element parent par rapport auquel se fait le déplacement.
	 * @param int $level
	 */
	public function getMaxChildrenLevelIfMovedAsFirstChildOf($parentEntity)
	{
		$deplacement = ($parentEntity->getLvl() - 1) - $this->getLvl();
		return $this->getMaxChildrenLevel() + $deplacement;
	}
	
	/**
	 * Retourne le niveau du plus petit des enfants si l'élément était déplacé via un moveAsNextSiblingOf.
	 * Utile pour s'assurer que le déplacement d'un élément n'a pas pour
	 * conséquence de dépasser une éventuelle limite de profondeur de l'arbre.
	 *
	 * @param object $siblingEntity Element frère par rapport auquel se fait le déplacement.
	 * @param int $level
	 */
	public function getMaxChildrenLevelIfMovedAsNextSiblingOfAction($siblingEntity)
	{
		$deplacement = $siblingEntity->getLvl() - $this->getLvl();
		$result = $this->getMaxChildrenLevel() + $deplacement;
		return $this->getMaxChildrenLevel() + $deplacement;
	}
}
