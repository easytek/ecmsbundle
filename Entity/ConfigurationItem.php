<?php

namespace Easytek\EcmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Easytek\EcmsBundle\Repository\ConfigurationItemRepository")
 * @ORM\Table(name="ecms_configuration_item")
 */
class ConfigurationItem
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
    /**
     * @var string $bundleName
     *
     * @ORM\Column(name="bundleName", type="string", length=255)
     */
	private $bundleName;
	
    /**
     * @var string $key
     *
     * @ORM\Column(name="k", type="string", length=255)
     */
	private $key;
	
    /**
     * @var string $value
     *
     * @ORM\Column(name="v", type="string", length=255)
     */
	private $value;
	
	/**
     * @var string $description
     */
	private $description;
	
    /**
     * @var string $type Type de champ de formulaire à utiliser.
	 */
	private $type;
	
	 /**
     * @var array $options Options du champ de formulaire à utiliser.
	 */
	private $options = array();
	
	
	public function getBundleName()
	{
		return $this->bundleName;
	}
	
	public function getKey()
	{
		return $this->key;
	}
	
	public function getValue()
	{
		return $this->value;
	}
	
	public function getDescription()
	{
		return $this->description;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function getOptions()
	{
		return $this->options;
	}
	
	public function setBundleName($bundleName)
	{
		$this->bundleName = $bundleName;
		return $this;
	}

	public function setKey($key)
	{
		$this->key = $key;
		return $this;
	}
	
	public function setValue($value)
	{
		$this->value = $value;
		return $this;
	}
	
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}
	
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}
	
	public function setOptions($options)
	{
		$this->options = $options;
		return $this;
	}
	
	public function addOption($key, $value)
	{
		$this->options[$key] = $value;
		return $this;
	}
}

?>
