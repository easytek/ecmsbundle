<?php

namespace Easytek\EcmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Easytek\EcmsBundle\Entity\Page
 *
 * @ORM\Table(name="ecms_page")
 * @ORM\Entity(repositoryClass="Easytek\EcmsBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @var integer $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min="0", max="64")
     */
    private $internalTitle;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(min="0", max="64")
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=2, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Length(min="2", max="2")
     */
    private $locale = 'fr';

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     *
     * @Assert\NotBlank()
     */
    private $html;

    /**
     * @var \DateTime $created
     *
     * @ORM\Column(type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @ORM\Column(type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    /**
     * @var string $slug
     *
     * @ORM\Column(type="string", length=128, nullable=false)
     * @Gedmo\Slug(fields={"internalTitle"})
     */
    private $slug;

    /**
     * @var string $template
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $template;

    /**
     * @var string $layout
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $layout;

    /**
     * @var boolean $linkable
     *
     * @ORM\Column(type="boolean")
     */
    private $linkable = true;

    /**
     * @var boolean $template
     *
     * @ORM\Column(type="boolean")
     */
    private $embeded = false;

    /**
     * @var boolean $template
     *
     * @ORM\Column(type="boolean")
     */
    private $homepage = false;


    public function getId()
    {
        return $this->id;
    }

    public function setInternalTitle($internalTitle)
    {
        $this->internalTitle = $internalTitle;
        return $this;
    }

    public function getInternalTitle()
    {
        return $this->internalTitle;
    }

    public function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function __toString()
    {
        return $this->getInternalTitle();
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    public function getLayout()
    {
        return $this->layout;
    }

    public function setLayout($layout)
    {
        $this->layout = $layout;
        return $this;
    }

    public function getLinkable()
    {
        return $this->linkable;
    }

    public function setLinkable($linkable)
    {
        $this->linkable = $linkable;
        return $this;
    }

    public function getEmbeded()
    {
        return $this->embeded;
    }

    public function setEmbeded($embeded)
    {
        $this->embeded = $embeded;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * @param bool $homepage
     * @return \Easytek\EcmsBundle\Entity\Page
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isHomepage()
    {
        return $this->getHomepage();
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function setDescription($description)
    {
        return $this->description = $description;
    }

    public function hasDescription()
    {
        return $this->description !== null && $this->description !== '';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function setTitle($title)
    {
        return $this->title = $title;
    }

    public function hasTitle()
    {
        return $this->title !== null && $this->title !== '';
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }
}
