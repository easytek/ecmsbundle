<?php

namespace Easytek\EcmsBundle\Service;

use Symfony\Bundle\FrameworkBundle\Routing\Router;

abstract class Linkable
{
	/**
	 * Doctrine Entity Manager
	 * @var Doctrine\ORM\EntityManager
	 */
	protected $em;
	/**
	 * Symfony Router
	 * @var Symfony\Bundle\FrameworkBundle\Routing\Router
	 */
	protected $router;
	/**
	 * Liste des routes possibles
	 * @var array
	 */
	protected $routes;
	/**
	 * Liste des routes du menu d'admin
	 * @var array
	 */
	protected $adminRoutes = array();
	/**
	 * Nom qui sera utilisé comme premier niveau dans le <select>
	 * @var string
	 */
	protected $publicName = 'undefined';
	
	/**
	 * Retourne un array de route serializées.
	 * 
	 * @return array
	 */
	public function getRoutes()
	{
		return $this->routes;
	}
	
	/**
	 * @return array
	 */
	protected function addRoute($humanName, $name, $parameters = array())
	{
		$value = json_encode(array(
			'name' => $name,
			'parameters' => $parameters
		));
		
		$this->routes[$value] = $humanName;
	}
	
	/**
	 * @return array
	 */
	protected function addAdminRoute($humanName, $name, $parameters = array())
	{
		$this->adminRoutes[] = array(
			'humanName' => $humanName,
			'name' => $name,
			'parameters' => $parameters
		);
	}
	
	public function getAdminRoutes()
	{
		return $this->adminRoutes;
	}
	
	/**
	 * Retourne le "nom public"
	 * 
	 * @return string
	 */
	public function getPublicName()
	{
		return $this->publicName;
	}
}
