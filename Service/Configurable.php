<?php

namespace Easytek\EcmsBundle\Service;

use Easytek\EcmsBundle\Service\Configurator;
use Easytek\EcmsBundle\Entity\ConfigurationItem;
use Doctrine\Common\Collections\ArrayCollection;

abstract class Configurable
{
	/**
	 * @var ArrayCollection
	 */
	protected $configurationItems;
	
	public function __construct()
	{
		$this->configurationItems = new ArrayCollection();
	}
	
	public function getConfigurationItems()
	{
		return $this->configurationItems;
	}
	
	public function addConfigurationItem(ConfigurationItem $configurationItem)
	{
		$this->getConfigurationItems()->add($configurationItem);
	}
}