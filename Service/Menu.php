<?php

namespace Easytek\EcmsBundle\Service;

/**
 * Service centralisant les services "linkables", ceux ci fournissant des contenus vers lesquels le menu peut faire des liens.
 */

class Menu
{
	private $services = array();
	
	public function getServices()
	{
		return $this->services;
	}
	
	public function addService($service)
	{
		$this->services[] = $service;
	}
	
	public function getUriChoice()
	{
		$uriChoice = array();
		
		foreach ($this->getServices() as $service) {
			$uriChoice[$service->getPublicName()] = $service->getRoutes();
		}
		
		return $uriChoice;
	}
	
	public function getAdminMenu()
	{
		$adminMenu = array();
		
		foreach ($this->getServices() as $service) {
			$adminMenu = array_merge($adminMenu, $service->getAdminRoutes());
		}
		
		return $adminMenu;
	}
}

?>
