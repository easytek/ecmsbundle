<?php

namespace Easytek\EcmsBundle\Service;

use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormTypeInterface;
use Easytek\EcmsBundle\Entity\ConfigurationItem;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;

use Easytek\EcmsBundle\Form\Configurator\ConfigurationType;

class Configuration
{
	/**
	 * @var ArrayCollection
	 */
	protected $configurationItems;
	protected $services = array();
	
	public function __construct(Registry $doctrine)
	{
		$this->configurationItems = new ArrayCollection();
		$configurationItems = $doctrine->getManager()->getRepository('EasytekEcmsBundle:ConfigurationItem')->findAll();
		
		// Doctrine ne retourne pas d'ArrayCollection en résultat mais de simple array, qui est ici converti.
		foreach ($configurationItems as $configurationItem) {
			$this->configurationItems->add($configurationItem);
		}
	}
	
	/**
	 * Retourne un ConfigurationItem correspondant à une key.
	 * La recherche se fait dans les objets persistés, si aucun résultat, un nouvel objet est instancié.
	 * @param type $key
	 * @return ConfigurationItem 
	 */
	public function getConfigurationItem($bundleName, $key)
	{
		foreach ($this->configurationItems as $configurationItem) {
			if ($configurationItem->getKey() == $key) {
				return $configurationItem;
			}
		}

		// Aucun item de configuration n'a été trouvé
		
		$newConfigurationItem = new ConfigurationItem();
		
		$newConfigurationItem
			->setKey($key)
			->setBundleName($bundleName)
		;
		
		$this->getConfigurationItems()->add($newConfigurationItem);
		
		return $newConfigurationItem;
	}
	
	public function addService($service)
	{
		$this->services[] = $service;
	}
	
	/**
	 * @return ArrayCollection
	 */
	public function getConfigurationItems()
	{
		return $this->configurationItems;
	}
	
	/**
	 * Similaire à getConfigurationItem, mais ne retourne pas un objet ConfigurationItem mais directement la "value" associé à cette "key".
	 * Exemple : $this->get('ecms.configuration')->get('contact.to'); // retourne "contact@easytek.fr"
	 * @param type $key 
	 */
	public function get($bundleName, $key)
	{
		return $this->getConfigurationItem($bundleName, $key)->getValue();
	}
}

?>
