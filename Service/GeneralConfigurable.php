<?php

namespace Easytek\EcmsBundle\Service;

use Easytek\EcmsBundle\Service\Configuration;
use Easytek\EcmsBundle\Entity\ConfigurationItem;

class GeneralConfigurable extends Configurable
{
	public function __construct(Configuration $configuration)
	{
		parent::__construct();
		
		$bundle = (string) $this;
		
		$key = 'title';
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue('ECMS');
		
		$ci
			->setKey($key)
			->setBundleName($bundle)
			->setType('text')
			->setOptions(array(
				'label' => 'Titre de la fenêtre'
			))
		;
		$this->addConfigurationItem($ci);
		
		//---
		
		$key = 'subtitle';
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue('ECMS');
		
		$ci
			->setKey($key)
			->setBundleName($bundle)
			->setType('text')
			->setOptions(array(
				'label' => 'Deuxième partie du titre de la fenêtre'
			))
		;
		$this->addConfigurationItem($ci);
		
		//---
		
		$key = 'h1';
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue('Lorem ipsum plop');
		
		$ci
			->setKey($key)
			->setBundleName($bundle)
			->setType('text')
			->setOptions(array(
				'label' => 'Nom du site'
			))
		;
		$this->addConfigurationItem($ci);
		
		//---
		
		$key = 'email_exp';
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue('no-reply@easytek.fr');
		
		$ci
			->setKey($key)
			->setBundleName($bundle)
			->setType('text')
			->setOptions(array(
				'label' => 'E-mail d\'expédition'
			))
		;
		$this->addConfigurationItem($ci);
		
		//---
		
		$key = 'email_dest';
		
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue('contact@easytek.fr');
		
		$ci
			->setKey($key)
			->setBundleName($bundle)
			->setType('text')
			->setOptions(array(
				'label' => 'Email destinataire'
			))
		;
		$this->addConfigurationItem($ci);
		
		//---
		
		
		$key = 'multilangue';
		
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue(true);
		
		$ci
		->setKey($key)
		->setBundleName($bundle)
		->setType('choice')
		->setOptions(array(
				'label' => 'Multilangue',
				'choices' => array(
						true => 'oui',
						false => 'non',
				),
		))
		;
		$this->addConfigurationItem($ci);
		
		//---
		
		$key = 'liveEdit';
		
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue(true);
		
		$ci
		->setKey($key)
		->setBundleName($bundle)
		->setType('choice')
		->setOptions(array(
			'label' => 'Page éditée en front-office',
			'choices' => array(
					true => 'oui',
					false => 'non',
			),
		))
		;
		$this->addConfigurationItem($ci);
		
	}
	
	public function __toString()
	{
		return 'general';
	}
}