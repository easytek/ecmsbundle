<?php

namespace Easytek\EcmsBundle\Service;

class PageLinkable extends Linkable
{
	public function __construct($doctrine)
	{
		$this->publicName = 'Pages';
		
		$pages = $doctrine->getManager()->getRepository('EasytekEcmsBundle:Page')->findByLinkable(true);
		
		// TODO : tester si site multilangue ou pas pour décider de la route (ou trouver mieux)
		
		foreach ($pages as $page) {
			$this->addRoute(
				'(' . $page->getLocale() . ') ' . $page->getInternalTitle(),
				'ecms_i18n_page_show',
				array(
					'id' => $page->getId(),
					'slug' => $page->getSlug(),
				)
			);
		}
	}
}
