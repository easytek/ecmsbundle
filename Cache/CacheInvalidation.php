<?php

namespace Easytek\EcmsBundle\Cache;

use Easytek\DoctrineCacheInvalidatorBundle\Cache\CacheInvalidationInterface;
use Easytek\DoctrineCacheInvalidatorBundle\Cache\CacheInvalidator;

class CacheInvalidation implements CacheInvalidationInterface
{
	public function getClasses()
	{
		return array(
			'Easytek\EcmsBundle\Entity\MenuItem' => array(
				array(
					'pattern' => 'menu_by_root_id_{parentId}',
					'changes' => array('*')
				),
				array(
					'pattern' => 'menu_by_root_name_{parentName}',
					'changes' => array('*')		
				)
			),
			'Easytek\EcmsBundle\Entity\Page' => array(
				array(
					'pattern' => 'page_{id}',
					'changes' => array('update', 'delete')
				),
				array(
					'pattern' => 'page_home',
					'changes' => array('update', 'delete')
				)
			),
			'Easytek\EcmsBundle\Entity\ConfigurationItem' => array(
				array(
					'pattern' => 'configuration',
					'changes' => array('*')
				)
			)
		);
	}
}