<?php

namespace Easytek\EcmsBundle\Menu;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Knp\Menu\FactoryInterface;
use Knp\Menu\Provider\MenuProviderInterface;
use Knp\Menu\ItemInterface;

class MenuProvider implements MenuProviderInterface
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;
	
    /**
     * @var FactoryInterface
     */
    protected $factory = null;

    /**
     * @param ContainerInterface $container
     * @param FactoryInterface $factory
     */
    public function __construct(ContainerInterface $container, FactoryInterface $factory)
    {
    	$this->container = $container;
        $this->factory = $factory;
    }

    /**
     * Retrieves a menu by its name
     *
     * @param string $name
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     * @throws \InvalidArgumentException if the menu does not exists
     */
    public function get($name, array $options = array())
    {
		$menu = $this->factory->createItem('root', array('childrenAttributes' => array('class' => 'nav')));

		$menuItems = $this->container->get('doctrine')->getRepository('EasytekEcmsBundle:MenuItem')->getMenuByRootName($name);
		
		if ($menuItems === null) {
			throw new \Exception('Menu "'.$name.'" not found.');
		}
		
		$this->build($menu, $menuItems[0]->getChildren());
		
		$menu->setCurrentUri($this->container->get('request')->getRequestUri());
		
		return $menu;
    }
    
    /**
     * Recursive function that convert a MenuItem NestedSet to a knp menu
     * @param ItemInterface $menu
     * @param array $menuItems
     */
    public function build(ItemInterface &$menu, $menuItems)
    {
    	foreach ($menuItems as $menuItem) {
    		if ($menuItem->hasRoute()) {
    			$node = $menu->addChild($menuItem->getName(), array(
					'route' => $menuItem->getRouteName(),
					'routeParameters' => $menuItem->getRouteParameters()
    			));
    		} else {
    			$node = $menu->addChild($menuItem->getName())->setUri($menuItem->getUri());
    		}
    		
    		if ($menuItem->getRole() != null) {
    			if ($this->container->get('security.context')->getToken() != null) {
    				$node->setDisplay($this->container->get('security.context')->isGranted($menuItem->getRole()));
    			} else {
    				$node->setDisplay(false);
    			}
    		}
    		
    		if ($menuItem->hasChildren()) {
    			$node->setAttribute('class', 'dropdown');
    			$node->setExtra('carret', true);
    			
    			$node->setLinkAttribute('class', 'dropdown-toggle');
    			$node->setLinkAttribute('data-toggle', 'dropdown');
    			
    			$node->setLabelAttribute('class', 'dropdown-toggle');
    			$node->setLabelAttribute('data-toggle', 'dropdown');
    			
    			$node->setChildrenAttribute('class', 'dropdown-menu');
    			$this->build($node, $menuItem->getChildren());
    		}
    	}
    }

    /**
     * Checks whether a menu exists in this provider
     *
     * @param string $name
     * @param array $options
     * @return bool
     */
    public function has($name, array $options = array())
    {
        return true;
    }
}